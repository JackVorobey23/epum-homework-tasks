import getValidateUsersFromRequest from './getUsersFromRequest.js';

import Compare from './compareUsers.js';

import findTeacher from './FindTeachers.js';
import getPercent from './getPercentOfTeachers.js';

import fillStatistics from './fillStatistics.js';

import sortTeachersBy from './sortTeachersBy.js';
import searchTeachers from './searchTeachers.js';
import addTeacher from './addTeacher.js';
import fillTopProfiles from './fillTopProfiles.js';
import fillFavProfiles from './fillFavProfiles.js';

import TeachersFilters from './TeachersFilters.js';
import getValidateCountriesFromRequest from './getPhonesFromRequest.js';

import showStatsNav from './showStatsNav.js';

import configurePieCharts from './configurePieCharts.js';

window.teachers = Compare(await getValidateUsersFromRequest(100));
window.topTeachers = window.teachers;

configurePieCharts();

let countries = await getValidateCountriesFromRequest();
window.map = L.map('map').setView([1, 1], 13);

console.log(findTeacher(window.teachers,'gender','male'));

let ageCondition = (teacher) => {
    return teacher.age >= 40;
}

console.log(getPercent(window.teachers, ageCondition));

fillStatistics(window.teachers);


const statsIds = ['full_name', 'course', 'age', 'gender', 'country'];

statsIds.forEach(statsId => {

    let elem = document.getElementById(`stats_a_${statsId}`);

    elem.addEventListener('click', () => sortTeachersBy(statsId), false);
});

document.getElementById('button-search').addEventListener('click', () => searchTeachers(teachers), false)

document.getElementById('add-teacher-form').addEventListener('submit', (event) => addTeacher(event), false)

fillTopProfiles(teachers);

fillFavProfiles(teachers);

showStatsNav(teachers, 1);

new TeachersFilters().configureSortEvents(teachers);