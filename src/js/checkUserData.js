export default function checkUserData(user) {

    const paramsToCheck = ['full_name', 'gender', 'city', 'state'];
    
    for (const param of paramsToCheck) {
        if(badCharacterExist(user[param]) || incorrectFirstLetter(user[param])){

            return [false, `incorrect ${param}.`];
        }
    }
    if(!user.email.includes('@')) {

        return [false, 'incorrect email.'];
    }
    else if(incorrectFirstLetter(user.note)){

        return [false, 'incorrect note.'];
    }
    else if (user.full_name.split(' ').length !== 2) {
        
        return [false, 'incorrect full name.'];
    }
    else if (incorrectFirstLetter(user.full_name.split(' ')[1])) {
        
        return [false, 'incorrect surname.'];
    }
    return [true, ''];
}
function badCharacterExist(stringToCheck) {
    
    stringToCheck = [...stringToCheck];
    const badCharacters = '1234567890+$#%!';

    stringToCheck.forEach(char => {
        if(badCharacters.includes(char)){

            return false;
        }
        return true;
    });
}
function incorrectFirstLetter(str){
    
    if(str[0] !== str[0].toUpperCase()){
        return true;
    }
    return false
}