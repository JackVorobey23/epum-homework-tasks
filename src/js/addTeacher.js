import checkUserData from './checkUserData.js';

// import dateIodayjs from 'https://cdn.jsdelivr.net/npm/@date-io/dayjs@2.17/+esm'

import lodash from 'https://cdn.jsdelivr.net/npm/lodash@4.17.21/+esm';

export default function addTeacher(event) {
    event.preventDefault();
    // const currentDate = new dateIodayjs();

    // // Specify the target date '2002-04-05'
    // const targetDate = new dateIodayjs('2002-04-05');

    // // Calculate the difference
    // const differenceInDays = currentDate.getDiff(targetDate, targetDate, 'day');
    let postData = {};
    const fields = ['b_date', 'city', 'country', 'course', 'email', 'full_name', 'note', 'phone', 'bg_color']

    fields.forEach(f => {
        postData[f] = document.getElementById(`add-teacher-${f}`).value;
    })

    const gender = lodash.find([...document.getElementsByClassName('popup-gender')[0]
        .querySelectorAll('input')], i => i.checked).value;

    const age = new Date().getUTCFullYear() - Number(postData.b_date.split('-')[0]);



    postData = Object.assign(postData, {
        favorite: false,
        gender: gender,
        id: (Math.random() * 10000000).toString(),
        age: age,
        state: postData.city
    });
    
    const result = checkUserData(postData);
    console.log(result);
    
    if (!result[0]) {
        console.log(result[1]);
    }

    else{
        window.teachers.push(postData);
    }
}