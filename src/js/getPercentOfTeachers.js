export default function getPercent(teachers, condition) {

    let counter = 0;

    teachers.forEach(teacher => {
        if (condition(teacher)) {

            counter++;
        }
    });

    return counter === 0 ? 0 : ((counter / teachers.length) * 100).toFixed(2);
}