import CountryDTO from './DTOs/CountryDTO.js';
import DataValidation from './DataValidation.js';
import { env } from './env.js';

export default async function getValidateCountriesFromRequest() {

    const url = env.GET_COUNTRIES_LINK;
    const dataValidation = new DataValidation();

    let res = [];

    const properties = dataValidation.getAllProperties(new CountryDTO(), []);

    const apiResponse = await fetch(url);
    const response = await apiResponse.json();

    const countries = response;

    countries.forEach(country => {

        country = dataValidation.validateCountriesData(country);

        country = dataValidation.getAllData(country, {}, properties);

        res.push(country);
    });

    return res;
}