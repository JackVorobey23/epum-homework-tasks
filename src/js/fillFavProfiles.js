import fillProfilePopup from "./fillProfilePopup.js";
import profileConstruct from "./profileConstruct.js";
import lodash from 'https://cdn.jsdelivr.net/npm/lodash@4.17.21/+esm';

export default function fillFavProfiles(users) {

    const elementsToFill = [...document.getElementsByClassName("fav-profile")];

    let userIndex = 0;

    users = lodash.filter(users, user => user.favorite);

    elementsToFill.forEach(element => {

        const currentUser = users[userIndex];
        try {
            element.onclick = () => {

                fillProfilePopup(currentUser);
            };
            
            element.innerHTML = profileConstruct(currentUser);
            
            userIndex++;
        }
        catch (ex) {
            element.innerHTML = "";
        }
    });
}