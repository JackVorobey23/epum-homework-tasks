export default function findTeacher(teachers, prop, value) {

    let res = teachers.find(teacher => teacher[prop] === value);

    if (res !== undefined) {
        return res;
    }
    else {
        console.log('teachers not found');
    }
}