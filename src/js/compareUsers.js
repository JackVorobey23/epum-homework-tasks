import { additionalUsers } from "./env.js";
import DataValidation from './DataValidation.js';
import AddUserDTO from "./DTOs/AddUserDTO.js";
import checkUserData from "./checkUserData.js";

export default function compareUsers(users) {

    const dataValidation = new DataValidation();
    
    let AddUserDto = new AddUserDTO();
    let properties = dataValidation.getAllProperties(AddUserDto, []);

    let addUsers = [];

    additionalUsers.forEach(addUser => {

        let userToAdd = dataValidation.getAllData(addUser, {}, properties);

        userToAdd = dataValidation.validateAddUsersData(userToAdd);
        
        addUsers.push(userToAdd);
    });
    for (let i = 0; i < users.length; i++) {
        
        const toAdd = Object.assign(users[i], addUsers[i % addUsers.length]);
        
        const isUserValid = checkUserData(toAdd);
        
        if(isUserValid[0]){
            users[i] = toAdd;
        }
        else {
            console.log(isUserValid[1]);
        }
    }

    return users;
}