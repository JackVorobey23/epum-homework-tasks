import fillTopProfiles from './fillTopProfiles.js';

export default class TeachersFilters {

    constructor() { }

    checkConditions(conditions) {

        let rightTeachers = [];
        
        window.teachers.forEach(teacher => {  
            if (conditions.ageCondition(teacher.age) && 
                conditions.sexCondition(teacher.gender) &&
                conditions.countryCondition(teacher.country) &&
                conditions.favoritesCondition(teacher.favorite) &&
                conditions.photoCondition(teacher.picture_large)) {

                rightTeachers.push(teacher);
            }
        });
        window.topTeachers = rightTeachers;
        fillTopProfiles();
    }

    configureSortEvents() {

        let conditions = {
            ageCondition: () => true,
            sexCondition: () => true,
            countryCondition: () => true,
            favoritesCondition: () => true,
            photoCondition: () => true
        }

        document.getElementById('age-filter').addEventListener('change', function (event) {
            if(event.target.value === 'all') {
                
                conditions.ageCondition = () => true;
            }
            else {
                conditions.ageCondition = (age) => {
    
                    let limits = String(event.target.value).split('-');
    
                    return Number(limits[0]) <= age && Number(limits[1]) >= age;
                }
            }

            new TeachersFilters().checkConditions(conditions);

        }, false);

        document.getElementById('region-filter').addEventListener('change', function (event) {
            if(event.target.value === 'all') {
                
                conditions.countryCondition = () => true;
            } 
            else {
                conditions.countryCondition = (country) => {
    
                    return event.target.value.toLowerCase() === country.toLowerCase();
                }
            }
            new TeachersFilters().checkConditions(conditions);

        }, false);

        document.getElementById('sex-filter').addEventListener('change', function (event) {
            if(event.target.value === 'all') {
                
                conditions.sexCondition = () => true;
            } 
            else {
                conditions.sexCondition = (sex) => {
    
                    return event.target.value.toLowerCase() === sex.toLowerCase();
                }
            }
            new TeachersFilters().checkConditions(conditions);

        }, false);

        document.getElementById('photo-filter').addEventListener('change', function (event) {
            if(event.target.checked) {

                conditions.photoCondition = photo => photo !== '';
            }
            else {
                conditions.photoCondition = () => true;
            }
            new TeachersFilters().checkConditions(conditions);

        }, false);

        document.getElementById('favorites-filter').addEventListener('change', function (event) {
            if(event.target.checked){
                conditions.favoritesCondition = fav => fav;
            }
            else {
                conditions.favoritesCondition = () => true;
            }
            new TeachersFilters().checkConditions(conditions);

        }, false);
    }
}