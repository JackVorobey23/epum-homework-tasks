const chokidar = require('chokidar');
const { exec } = require('child_process');

const scssFilePath = './src/scss/index.scss';
const cssFilePath = './src/css/index.css';

console.log('scss watch running...');
// Function to execute the command when a file change is detected
const runSassCompiler = () => {
  const command = `npx sass ${scssFilePath} ${cssFilePath}`;

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error occurred: ${error.message}`);
      return;
    }

    console.log(stdout);
  });
};

// Initialize watcher to monitor the SCSS file
chokidar.watch(scssFilePath).on('change', () => {
  const time = Date.now();
  console.log(`File ${scssFilePath} has been changed. Running Sass compiler...`);
  runSassCompiler();
  console.log(`done in ${Date.now() - time}ms.`);
});