export default function profileConstruct(teacherInfo) {

    const starInfo = teacherInfo.favorite === true ?
        `<img src='images/star.png' alt='star' class='star-img'>` : '';

    let altText = teacherInfo.full_name.split(' ');

    const [firstName, lastName] = teacherInfo.full_name.split(' ');

    altText = `${firstName[0]}. ${lastName[0]}`;

    return `<a href='#popup1'><img src='${teacherInfo.picture_large}' class='prof-img' alt='${altText}'></a>
            ${starInfo}
            <p class='full-name'>${teacherInfo.full_name}</p>
            <p class='hobby'>${teacherInfo.course}</p>
            <p class='country'> ${teacherInfo.country}</p>`;
}