import { courses } from "./env.js";

export default class DataValidation{

    constructor() { }

    getAllProperties(object, res) {

        Object.getOwnPropertyNames(object).forEach((val) => {
            
            if(typeof object[val] === "object"){
    
                res = this.getAllProperties(object[val], res);
            }
            else {
                res.push(val);
            }
          });
        return res;
    }

    getAllData(data, result, properties) {
        for(const obj in data){
            if(typeof data[obj] === "object" && !properties.includes(obj)){
    
                result = this.getAllData(data[obj], result, properties);
            }
            else if (properties.includes(obj)){
    
                Object.assign(result, JSON.parse(`{"${obj}":${JSON.stringify(data[obj])}}`));
            }
        }
        return result;
    }

    validateTeacherData(toValidate) {

        delete toValidate["registered"];
        toValidate.b_date = toValidate.dob.date;
        toValidate.full_name = `${toValidate.name.first} ${toValidate.name.last}`;
        toValidate.picture_large = toValidate.picture.large;
        toValidate.picture_thumbnail = toValidate.picture.thumbnail;
    
        return toValidate;
    }

    validateAddUsersData(toValidate) {
        
        Object.keys(toValidate).forEach(key => {
    
            if(toValidate[key] === null){
                if(key === "bg_color"){
    
                    toValidate[key] = `#${Math.floor(Math.random()*16777215).toString(16)}`;
                }
                else if (key === "favorite"){
    
                    toValidate[key] = false;
                }
                else {
    
                    toValidate[key] = Math.random().toString(36).slice(2);
                }
            }
        });

        toValidate.course = courses[Math.floor(Math.random() * courses.length)];

        toValidate.note =  toValidate.note.charAt(0).toUpperCase() + toValidate.note.slice(1);
        
        return toValidate;
    }

    validateCountriesData(toValidate) {
        
        toValidate.country_name = toValidate.name.common;

        return toValidate;
    }
}