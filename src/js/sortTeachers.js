export default function sortTeachers (param, reverseOrder) {

    (window.teachers.sort((a, b) => 
    
        (a[param] > b[param]) ? 1 : ((b[param] === a[param]) ? 0 : -1)
    ));

    return reverseOrder ? 
        window.teachers.reverse() : window.teachers;
}
