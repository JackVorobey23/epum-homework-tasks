import SortTeachers from './SortTeachers.js';
import fillStatistics from './fillStatistics.js';

let sorts = {
    prev:'',
    prevPrev:'',
}

export default function sortTeachersBy(param) {
    
    let reversed = false;

    if(sorts.prev === param && sorts.prev !== sorts.prevPrev){

        reversed = true;
    }

    fillStatistics(SortTeachers(param, reversed));

    sorts.prevPrev = sorts.prev;
    sorts.prev = param;

    if(sorts.prev === sorts.prevPrev && sorts.prev !== ''){
        sorts.prev = '';    
    }
}