import profileConstruct from './profileConstruct.js';
import fillProfilePopup from './fillProfilePopup.js';

export default function fillTopProfiles() {

    const elementsToFill = [...document.getElementsByClassName('top-profile')];

    let userIndex = 0;

    elementsToFill.forEach(element => {

        const currentUser = window.topTeachers[userIndex];
        
        try {
            element.onclick = () => {
                fillProfilePopup(currentUser);
            };
            element.innerHTML = profileConstruct(currentUser);
            
            userIndex++;
        }
        catch (ex) {
            element.innerHTML = '';
        }
    });
}