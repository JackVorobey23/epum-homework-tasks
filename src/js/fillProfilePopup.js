import fillTopProfiles from './fillTopProfiles.js';

export default function fillProfilePopup(teacher) {
    
    const fields = ['full_name', 'course', 'email', 'phone', 'note']

    fields.forEach(field => {
        
        const popupField = document.getElementsByClassName(`ti-popup-${field}`)[0];
        
        popupField.innerText = teacher[field];
    });

    document.querySelector('.ti-popup-age-gender').innerText =
        `${teacher.age}, ${teacher.gender}`;
    
    document.querySelector('.ti-popup-city').innerText =
        `${teacher.city}, ${teacher.country}`;

    document.getElementById('person-image').src = teacher.picture_large;
    
    const starElement = document.getElementById('popup1').querySelector('.star-img');

    starElement.src = teacher.favorite ? 'images/star.png' : 'images/empty-star.png';

    window.map.off();
    window.map.remove();
    
    try {
        window.map = L.map('map').setView([teacher.coordinates.latitude, teacher.coordinates.longitude], 13);
    
        L.marker([teacher.coordinates.latitude, teacher.coordinates.longitude]).addTo(map);
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '© OpenStreetMap'
        }).addTo(window.map);
    }
    catch {
        console.log('');
    }
    
    starElement.onclick = () => {
        
        const personIndex = window.teachers.findIndex(t => t.full_name === teacher.full_name)
        
        window.teachers[personIndex].favorite = !window.teachers[personIndex].favorite;
        window.topTeachers = window.teachers;
        starElement.src = window.teachers[personIndex].favorite ? 'images/star.png' : 'images/empty-star.png';
        fillTopProfiles();
    }
}