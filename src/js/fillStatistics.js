export default function fillStatistics(teachers){
    
    const dataClasses = ['full_name', 'course', 'age', 'gender', 'country'];
    
    dataClasses.forEach(dataClass => {
        
        let userIndex = 0;
        
        const elementsToFill = [...document.getElementsByClassName(`stats_${dataClass}`)];

        elementsToFill.forEach(element => {
            try{
                element.innerText = teachers[userIndex++][dataClass];
            } 
            catch{
                console.log('user not found');
            }
        });
    });
}