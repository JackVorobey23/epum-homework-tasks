export default function configurePieCharts() {
    let ageMap = new Map();
    window.teachers.forEach(teacher => {

        if (ageMap.has(teacher.age)) {

            ageMap.set(teacher.age, ageMap.get(teacher.age) + 1);
        } else {

            ageMap.set(teacher.age, 1);
        }
    });
    ageMap = [...ageMap].sort((a, b) => a[1] - b[1]);

    const ageData = {
        
        labels: ageMap.map(_ => _[0]),
        
        datasets: [{
            label: 'My First Dataset',
            data: ageMap.map(_ => _[1]),
            backgroundColor: [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)',
                'rgb(255, 205, 86)'
            ],
            hoverOffset: 4
        }]
    };

    const genderMap = [0, 0]

    window.teachers.forEach(t => {
        if (t.gender === "Male") {
            
            genderMap[0]++;
        }
        else {
            genderMap[1]++;
        }
    });
    const genderData = {
        
        labels: ['Male', 'Female'],
        
        datasets: [{
            label: 'My First Dataset',
            data: genderMap,
            backgroundColor: [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)',
                'rgb(255, 205, 86)'
            ],
            hoverOffset: 4
        }]
    }
    const ageCtx = document.getElementById('ageChart');
    const genderCtx = document.getElementById('genderChart');

    new Chart(ageCtx, {
        type: 'doughnut',
        data: ageData
    });

    new Chart(genderCtx, {
        type: 'doughnut',
        data: genderData,
    });
    return ageData;
}