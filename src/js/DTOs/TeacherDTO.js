export default class TeacherDTO{
    gender; 
    title;  
    full_name;  
    city;  
    state;  
    country;  
    postcode;  
    coordinates;  
    timezone;  
    email;  
    b_date;  
    age;  
    phone;  
    picture_large;  
    picture_thumbnail;  
}