import TeacherDTO from './DTOs/TeacherDTO.js';
import DataValidation from './DataValidation.js';
import { env } from './env.js';

export default async function getValidateUsersFromRequest(amountOfTeachers) {

    const url = env.GET_TEACHER_LINK;
    const dataValidation = new DataValidation();

    let res = [];

    const properties = dataValidation.getAllProperties(new TeacherDTO(), []);

    const apiResponse = await fetch(url + `/?results=${amountOfTeachers}`);
    const response = await apiResponse.json();

    const users = response.results;

    users.forEach(user => {

        user = dataValidation.validateTeacherData(Object.assign({}, user));

        user = dataValidation.getAllData(user, {}, properties);

        if (Math.random() > 0.80) {
            user.picture_large = '';
        }

        const toUpperFrstLttr = (str) => str.charAt(0).toUpperCase() + str.slice(1);

        user.gender = toUpperFrstLttr(user.gender);
        user.full_name = toUpperFrstLttr(user.full_name);
        user.country = toUpperFrstLttr(user.country);
        user.state = toUpperFrstLttr(user.state);
        user.city = toUpperFrstLttr(user.city);
        user.phone = `+${user.phone}`;

        res.push(user);
    });

    return res;
}