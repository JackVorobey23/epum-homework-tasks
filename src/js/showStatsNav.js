import fillStatistics from './fillStatistics.js';

export default function showStatsNav(newFocus) {

    let VisibleAnchors = [1,2,3,'...','Last'];
    
    const maxPage = Math.ceil(window.teachers.length / 10);

    if(newFocus === 'First'){
        
        newFocus = 1;
    } 
    else if (newFocus === 'Last'){

        newFocus = maxPage;
    }
    else if (newFocus === '...'){

        return;
    } 
    else {
        newFocus = Number(newFocus);
    }

    fillStatistics(window.teachers.slice((newFocus - 1) * 10, newFocus * 10));

    if(newFocus === 1 || newFocus === 2){

        VisibleAnchors = [1,2,3,'...', 'Last'];
    } 
    else if (newFocus === maxPage || newFocus === maxPage - 1){

        VisibleAnchors = ['First', '...', maxPage - 2, maxPage - 1, maxPage];
    }
    else if (newFocus >= 3 && newFocus <= maxPage - 2){

        VisibleAnchors = ['First', '...', newFocus - 1, newFocus, newFocus + 1,'...', 'Last'];
    }

    const toFill = document.getElementById('statistics-nav');
    toFill.innerHTML = '';

    VisibleAnchors.forEach(anchor => {

        const active = anchor === newFocus ? `class='stat-nav-active'` : ``;

        toFill.innerHTML = toFill.innerHTML + 
        `<a ${active} value='${anchor}'>${anchor}</a>`;
        
    });

    [...toFill.children].forEach((child) => {
        
        child.onclick = function () {
            
            showStatsNav(this.getAttribute('value'));
            
        };
    });
}