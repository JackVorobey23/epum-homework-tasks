import fillTopProfiles from './fillTopProfiles.js';

export default function searchTeachers() {

    const search = document.getElementsByClassName('input-search')[0].value;

    if (search === '') {
        
        window.topTeachers = window.teachers;
        
        fillTopProfiles();
        return;
    }
    const topTeachers = new Set();

    if (isNaN(search)) {

        window.teachers.forEach(teacher => {
            if (String(teacher.full_name)
                .toLowerCase()
                .includes(search.toLowerCase())) {
                
                topTeachers.add(teacher);
            }
        })

        window.teachers.forEach(teacher => {
            if (String(teacher.note)
                .toLowerCase()
                .includes(search.toLowerCase())) {
                
                topTeachers.add(teacher);
            }
        })
    }

    else {
        const searchAge = Number(search);

        window.teachers.forEach(teacher => {
            if (teacher.age === searchAge) {
                topTeachers.add(teacher);
            }
        })
    }
    window.topTeachers = [...topTeachers];
    fillTopProfiles();
}